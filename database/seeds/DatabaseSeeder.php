<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Liebreiz",
            'email' => "admin@admin.com",
            'email_verified_at' => now(),
            'password' => '$2y$10$umdOzebLZ7Z4gFFaRkxBhe52cvX8Sbh4RkgUcwn9VVL0MMKY22bi.', // password
            'remember_token' => Str::random(10),
        ]);
    }
}
