<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Mail;
use App\Product;

class WebsiteController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function aboutUs()
    {
        return view('about_us');
    }
    public function news()
    {
        return view('news');
    }
    public function services()
    {
        return view('services');
    }
    public function carCosmetics()
    {
        return view('car_cosmetics');
    }
    public function foilTechnology()
    {
        return view('foil_technology');
    }
    public function rimTire()
    {
        return view('rim_tire');
    }
    public function voucher()
    {
        return view('voucher');
    }
    public function contact()
    {
        return view('contact');
    }
    public function portfolio()
    {
        return view('portfolio');
    }


    // public function product()
    // {
    //     $products = Product::get();
    //     return view('product',compact('products'));
    // }

    // public function contact()
    // {
    //     return view('contact');
    // }

    // public function work()
    // {
    //     return view('work');
    // }

    // public function email(Request $request)
    // {
    //     // dd(\Request::ip());
    //     \Mail::send('contact_email',
    //         [
    //             'name' => $request->get('name'),
    //             'email' => $request->get('email'),
    //             'phone' => $request->get('phone'),
    //             'country' => $request->get('country'),
    //             'company' => $request->get('company'),
    //             'projectdetails' => $request->get('projectdetails'),
    //         ],

    //         function ($message) use ($request) {
    //             $message->from('info@multipz.com');
    //             $message->bcc('multipz.hitendrasoni@gmail.com');
    //             $message->to('hitesh.devit@gmail.com')->subject('Inquiry from Customer');
    //         }
    //     );

    //     return redirect()->back()->with(['success' => 'Thank you for contact with us!']);
    // }
}    