<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product->title = $request->title;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->category = $request->category;
        
        if ($request->file('image')) {
            $storage_path = 'public/images';
            $product->image = Storage::disk('local')->put($storage_path,$request->file('image'));
        }

        $product->save();
        return redirect()->route('products.index')->with('message', "Product added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->title = $request->title;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->category = $request->category;
        
        if ($request->file('image')) {
            Storage::disk('local')->delete($product->image);

            $storage_path = 'public/images';
            $product->image = Storage::disk('local')->put($storage_path,$request->file('image'));
        }

        $product->save();
        return redirect()->route('products.index')->with('message', "Product update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $products = Product::find(request('ids'));
        $products->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $crop = Product::find($request->ids);
        if (empty($crop->is_active)) {
            $crop->is_active = 1;
        } else {
            $crop->is_active = 0;
        }
        $crop->save();
        return response()->noContent();
    }
}
