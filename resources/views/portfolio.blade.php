@section('title', 'Portfolio')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
    <div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
                <img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content p-bot89 col-12">
							<h1>Our Portfolio</h1>
							<ul>
								<li><a href="{{ route('index')}}">HOME</a></li>
								<li class="current text-light">OUR PORTFOLIO</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1">
				<div class="container-fluid">
					<div class="owl-carousel owl-theme">
						<div class="item">
							<div class="row">
								<div class="list-thumbnail col-12 col-md-2">
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-1.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-2.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-3.jpg') }}" alt="autokosmetik">
										</a>
									</div>
								</div>
								<div class="contents col-12 col-md-10">
									<div class="main-image-detail">
										<ul>
											<li>
												<img class="img-fluid src-image" src="{{ asset('assets/frontend/images/port-large-1.jpg') }}" alt="autokosmetik">
											</li>
										</ul>
									</div>
									<div class="row">
										<div class="project-description col-12 col-sm-8 pt-3">
											<h3>Project description</h3>
											<p>The PSD file includes a smart layer so all you have to do is place your design inside it and you’ll be done in no time.  Aliquam vehicula mollis urna vel dignissim.<br><br>Integer tincidunt viverra est, non congue lorem tempor ac. Phasellus pulvi-nar iaculis nunc at placerat. Sed porta sollicitudin eros, vel sagittis turpis consequat nec. 
											</p>
										</div>
										<div class="sidebar-description col-12 col-sm-4 pt-3">
											<div class="item">
												<h4>Created:</h4>
												<p>March 17th, 2019</p>
											</div>
											<div class="item">
												<h4>Client:</h4>
												<p>March 17th, 2019</p>
											</div>
											<div class="item">
												<h4>Category:</h4>
												<p>Illustration, <span>Brand</span></p>
											</div>
										</div>
										<div class="shared ez-animate col-12" data-animation="fadeInUp">
											<h4>Shared:</h4>
											<div class="social-links">
												<a href="#"><i class="fa fa-facebook fa-lg"></i></a>
												<a href="#"><i class="fa fa-twitter fa-lg"></i></a>
												<a href="#"><i class="fa fa-instagram fa-lg"></i></a>
												<a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="list-thumbnail col-12 col-md-2">
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-1.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-2.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-3.jpg') }}" alt="autokosmetik">
										</a>
									</div>
								</div>
								<div class="contents col-12 col-md-10">
									<div class="main-image-detail">
										<ul>
											<li>
												<img class="img-fluid src-image" src="{{ asset('assets/frontend/images/port-large-1.jpg') }}" alt="autokosmetik">
											</li>
										</ul>
									</div>
									<div class="row">
										<div class="project-description col-12 col-sm-8 pt-3">
											<h3>Project description</h3>
											<p>The PSD file includes a smart layer so all you have to do is place your design inside it and you’ll be done in no time.  Aliquam vehicula mollis urna vel dignissim.<br><br>Integer tincidunt viverra est, non congue lorem tempor ac. Phasellus pulvi-nar iaculis nunc at placerat. Sed porta sollicitudin eros, vel sagittis turpis consequat nec. 
											</p>
										</div>
										<div class="sidebar-description col-12 col-sm-4 pt-3">
											<div class="item">
												<h4>Created:</h4>
												<p>March 17th, 2019</p>
											</div>
											<div class="item">
												<h4>Client:</h4>
												<p>March 17th, 2019</p>
											</div>
											<div class="item">
												<h4>Category:</h4>
												<p>Illustration, <span>Brand</span></p>
											</div>
										</div>
										<div class="shared ez-animate col-12" data-animation="fadeInUp">
											<h4>Shared:</h4>
											<div class="social-links">
												<a href="#"><i class="fa fa-facebook fa-lg"></i></a>
												<a href="#"><i class="fa fa-twitter fa-lg"></i></a>
												<a href="#"><i class="fa fa-instagram fa-lg"></i></a>
												<a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="list-thumbnail col-12 col-md-2">
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-1.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-2.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void(0);">
											<img id="my-img" src="{{ asset('assets/frontend/images/port-large-3.jpg') }}" alt="autokosmetik">
										</a>
									</div>
								</div>
								<div class="contents col-12 col-md-10">
									<div class="main-image-detail">
										<ul>
											<li>
												<img class="img-fluid src-image" src="{{ asset('assets/frontend/images/port-large-1.jpg') }}" alt="autokosmetik">
											</li>
										</ul>
									</div>
									<div class="row">
										<div class="project-description col-12 col-sm-8 pt-3">
											<h3>Project description</h3>
											<p>The PSD file includes a smart layer so all you have to do is place your design inside it and you’ll be done in no time.  Aliquam vehicula mollis urna vel dignissim.<br><br>Integer tincidunt viverra est, non congue lorem tempor ac. Phasellus pulvi-nar iaculis nunc at placerat. Sed porta sollicitudin eros, vel sagittis turpis consequat nec. 
											</p>
										</div>
										<div class="sidebar-description col-12 col-sm-4 pt-3">
											<div class="item">
												<h4>Created:</h4>
												<p>March 17th, 2019</p>
											</div>
											<div class="item">
												<h4>Client:</h4>
												<p>March 17th, 2019</p>
											</div>
											<div class="item">
												<h4>Category:</h4>
												<p>Illustration, <span>Brand</span></p>
											</div>
										</div>
										<div class="shared ez-animate col-12" data-animation="fadeInUp">
											<h4>Shared:</h4>
											<div class="social-links">
												<a href="#"><i class="fa fa-facebook fa-lg"></i></a>
												<a href="#"><i class="fa fa-twitter fa-lg"></i></a>
												<a href="#"><i class="fa fa-instagram fa-lg"></i></a>
												<a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1">
				<div class="container">
					<!-- Related Project -->
					<div class="related-projects col-12">
						<h2>Related projects</h2>
						<div class="row">
							<!-- Item -->
							<div class="item col-sm-12 col-md-4">
								<a href="#">
									<div class="img-container">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/related-1.jpg') }}" alt="autokosmetik">
										<div class="overlay">
											<div class="overlay-content">
												<i></i>
												<h3>Autokosmetik</h3>
												<p>Photography</p>
												<span></span>
											</div>
										</div>
									</div>
								</a>
							</div>
							<!-- /.Item -->
							<!-- Item -->
							<div class="item col-sm-12 col-md-4">
								<a href="#">
									<div class="img-container">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/related-2.jpg') }}" alt="autokosmetik">
										<div class="overlay">
											<div class="overlay-content">
												<i></i>
												<h3>Autokosmetik</h3>
												<p>Photography</p>
												<span></span>
											</div>
										</div>
									</div>
								</a>
							</div>
							<!-- /.Item -->
							<!-- Item -->
							<div class="item col-sm-12 col-md-4">
								<a href="#">
									<div class="img-container">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/related-3.jpg') }}" alt="autokosmetik">
										<div class="overlay">
											<div class="overlay-content">
												<i></i>
												<h3>Autokosmetik</h3>
												<p>Photography</p>
												<span></span>
											</div>
										</div>
									</div>
								</a>
							</div>
							<!-- /.Item -->
						</div>
					</div>
					<!-- /.Related Project -->
					<!-- Load More -->
					<div class="loadmore-btn col-12 text-center">
						<a href="#" class="btn-1">See all work</a>
					</div>
				</div>
			</div>
			<!-- /.Section Portfolio Details 1 -->

		</div>
	</div>
@endsection    
	