@section('title', 'Contact')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<!-- Section Breadcrumb 1 -->
		<div id="section-breadcrumb1" class="inner-banner-wrap">
			<img src="{{ asset('assets/frontend/images/contact-banner.jpg') }}" alt="" class="inner-page-banner">
			<div class="container">
				<div class="row">
					<div class="content col-12">
						<h1>Contact Us</h1>
						<ul>
							<li><a href="{{ route('index')}}">HOME</a></li>
							<li class="current text-light"><a href="#">CONTACT US</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.Section Breadcrumb 1 -->
		<!-- Section Contact -->
		<div id="section-contact">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1>Get in Touch</h1>
					</div>
					<!-- Contact Form -->
					<div class="contact-form col-sm-12 col-md-6">
						<!-- Form -->
						<form action="#">
							<div class="row">
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="text" class="form-control" name="name" placeholder="Name" required="">
								</div>
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="email" class="form-control" name="name" placeholder="Email*" required="">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number*" required="">
								</div>
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="email" class="form-control" name="subject" placeholder="Subject" required="">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-12">
									<textarea class="form-control" name="message" placeholder="Message" required=""></textarea>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<div class="checkbox">
										<input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
										<label for="styled-checkbox-1">I am not a robot <i class="flaticon-user"></i></label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<button type="submit">Submit</button>
								</div>
							</div>
						</form>
						<!-- /.Form -->
					</div>
					<!-- /.Contact Form -->
					<!-- Contact Details -->
					<div class="contact-details col-sm-12 col-md-6">
						<!-- Item -->
						<div class="item ez-animate" data-animation="fadeInRight">
							<div class="thumb-icon">
								<i class="flaticon-house"></i>
							</div>
							<div class="description">
								<a href="https://www.google.ch/maps/place/Autokosmetik.li/@47.218252,9.511339,15z/data=!4m5!3m4!1s0x0:0x3025938a37e377f0!8m2!3d47.218252!4d9.511339" target="_blank">
									<p> Haldenstrasse 90, 9487 Gamprin,
									<br>Principality of Liechtenstein</p>
								</a>
							</div>
						</div>
						<!-- /.Item -->
						<!-- Item -->
						<div class="item ez-animate" data-animation="fadeInRight">
							<div class="thumb-icon">
								<i class="flaticon-smartphone-1"></i>
							</div>
							<div class="description">
								<p>Office: <a href="callto:00423 373 03 54">+423 373 03 54</a><br>Mobile: <a href="callto:00423 792 57 75">+423 792 57 75</a></p>
							</div>
						</div>
						<!-- /.Item -->
						<!-- Item -->
						<div class="item ez-animate" data-animation="fadeInRight">
							<div class="thumb-icon">
								<i class="flaticon-paper-plane"></i>
							</div>
							<div class="description">
								<p><a href="mailto:info@autokosmetik.li">info@autokosmetik.li</a></p>
							</div>
						</div>
						<!-- /.Item -->
					</div>
					<!-- /.Contact Details -->
				</div>
			</div>
		</div>
		<!-- /.Section Contact -->
		<!-- Section Map -->
		<div id="section-map">
			<div class="detail">
				<div class="marker-img">
					<img src="{{ asset('assets/frontend/images/marker.png') }}" alt="autokosmetik">
				</div>
				<div class="description">
					<!-- <h5>Australia Office</h5> -->
					<p> Haldenstrasse 90<br>
						9487 Gamprin<br>
						Principality of Liechtenstein</p>
				</div>
			</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10839.65866013981!2d9.511339!3d47.218252!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3025938a37e377f0!2sAutokosmetik.li!5e0!3m2!1sen!2sin!4v1613554901786!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			<!-- <div id="map"></div> -->
		</div>
		<!-- /.Section Map -->
	</div>
@endsection