@section('title', 'About Us')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<!-- Sidebar  -->
		<div id="main-content" class="active">
			<!-- Section Breadcrumb 1 -->
		    <div id="section-breadcrumb1" class="bg1 bg-dark-autokosmetic">
		    	<div class="container">
		    		<div class="row">
		    			<div class="content col-12">
		    				<h1>About Us</h1>
		    				<ul>
		    					<li><a href="{{ route('index')}}">HOME</a></li>
		    					<li class="current text-light">ABOUT US</li>
		    				</ul>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div id="section-allstarted">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-6" data-aos="fade-up">
							<h1>Autokosmetik.li Anstalt is owned by <strong>Pascal and Rudi Nitzlnader.</strong></h1>
							<p>
								The company has existed since 2008 and was converted from a simple partnership into an institution in early 2014. Father and son run the company together.
								<br>
								"It's the small things that make perfection, but perfection is anything but a small thing" - <strong>Sir Frederick Henry Royce</strong>
							</p>
						</div>
						<div class="right col-sm-12 col-md-6">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/about-1.png') }}" alt="autokosmetik">
						</div>
						<div class="space113"></div>
						<div class="strategic-left col-sm-12 col-md-12 col-lg-7" data-aos="fade-up" data-aos-delay="200">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-allstarted_2.png') }}" alt="autokosmetik">
						</div>
						<div class="strategic-right col-sm-12 col-md-12 col-lg-5" >
							<div class="item style1 bg-dark-autokosmetic" data-aos="fade-up" data-aos-delay="400">
								<i class="flaticon-network"></i>
								<h3>Strategic Vision</h3>
								<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut molestiae perferendis</p>
							</div>
							<div class="item style2 bg-dark-autokosmetic" data-aos="fade-up" data-aos-delay="800">
								<i class="flaticon-diamond"></i>
								<h3>Mission</h3>
								<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut molestiae perferendis</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-testimonial1" class="">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 col-12" data-aos="fade-up">
								<h2><span>The Opinion our</span> Clients</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme" data-aos="fade-up">
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum dolor</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial2.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum dolor</h6>
										<p>Lorem ipsum </p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial3.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum dolor</h6>
										<p>Lorem ipsum </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-ourteam1">
				<div class="container">
					<div class="row ez-animate" data-animation="fadeInUp">
						<div class="title1 col-12">
							<h2><span>Meet the</span> Collaborators</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="item ol-sm-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="200">
							<a href="#">
								<div class="content">
									<h3>Lorem ipsum dolor</h3>
									<p>Lorem ipsum</p>
								</div>
								<div class="image">
									<img class="img-fluid" src="{{ asset('assets/frontend/images/img-ourteam1.jpg') }}" alt="autokosmetik">
								</div>
							</a>
						</div>
						<div class="item ol-sm-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="400">
							<a href="#">
								<div class="content">
									<h3>Lorem ipsum dolor</h3>
									<p>Lorem ipsum</p>
								</div>
								<div class="image">
									<img class="img-fluid" src="{{ asset('assets/frontend/images/img-ourteam2.jpg') }}" alt="autokosmetik">
								</div>
							</a>
						</div>
						<div class="item ol-sm-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="600">
							<a href="#">
								<div class="content">
									<h3>Lorem ipsum dolor</h3>
									<p>Lorem ipsum</p>
								</div>
								<div class="image">
									<img class="img-fluid" src="{{ asset('assets/frontend/images/img-ourteam3.jpg') }}" alt="autokosmetik">
								</div>
							</a>
						</div>
						<div class="item ol-sm-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="800">
							<a href="#">
								<div class="content">
									<h3>Lorem ipsum dolor</h3>
									<p>Lorem ipsum</p>
								</div>
								<div class="image">
									<img class="img-fluid" src="{{ asset('assets/frontend/images/img-ourteam4.jpg') }}" alt="autokosmetik">
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="section-miscellaneous1">
				<div class="container">
					<div class="row">
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-heart"></i>
							<h3 class="counter-value count">288.702</h3>
							<p>Customers</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-stopwatch"></i>
							<h3 class="counter-value count">140.545</h3>
							<p>Works completes</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-internet"></i>
							<h3 class="counter-value count">100.204</h3>
							<p>Projects of design</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-id-card"></i>
							<h3 class="counter-value count">22.368</h3>
							<p>Companies</p>
						</div>
					</div>
				</div>
			</div>
			<div id="section-cta3">
				<div class="container">
					<div class="row ez-animate">
						<div class="title1 col-12" data-aos="fade-up" data-aos-delay="200">
							<h2><span>Always looking for more</span> Unique Talents</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="cta-content col-12" data-aos="fade-up" data-aos-delay="400">
							<p>All employees of Autokosmetik.li Anstalt are trained car beauticians who have been working in this profession for years.
								The love for perfect, beautiful cars, a very good and trained eye, a good physical condition and full
								concentration at work complete the picture of the employees.</p>
							<a href="#" class="btn-4">See job offers</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	