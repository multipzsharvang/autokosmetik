@section('title', 'Home')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
        <!-- Page Content  -->
        <div id="main-content" class="active">
            <!-- Revolution Slider 1 -->
            <div id="rev_slider_14_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="autokosmetik" data-source="gallery" style="background:#ffffff;padding:0px;">
				<!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
				<div id="rev_slider_14_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
					<ul>	<!-- SLIDE  -->
					<li data-index="rs-49" data-transition="slidingoverlayright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="{{ asset('assets/frontend/revslider1/assets/100x50_9e6da-slider1.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
						<!-- MAIN IMAGE -->
						<img src="{{ asset('assets/frontend/revslider1/assets/9e6da-slider1.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						<div class="tp-caption"
							id="slide-49-layer-3"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-72','-47']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;"><strong>Autokosmetik</strong>  is the perfect </div>
						<!-- LAYER NR. 2 -->
						<div class="tp-caption  "
							id="slide-49-layer-4"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">for all <strong>types</strong> </div>
						<!-- LAYER NR. 3 -->
						<div class="tp-caption"
							id="slide-49-layer-5"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['81','83','70','45']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">of <strong>solutions</strong> </div>
						<!-- LAYER NR. 4 -->
						<div class="tp-caption  "
							id="slide-49-layer-7"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-143','-147','-138','-91']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 8; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><i class="pe-7s-glasses"></i> </div>
						<!-- LAYER NR. 5 -->
						<a class="tp-caption scroll-down"
							href="#section-aboutus1" target="_self"	id="slide-49-layer-9"
							data-x="['right','right','right','right']" data-hoffset="['-38','-38','-38','-38']"
							data-y="['middle','middle','middle','middle']" data-voffset="['188','196','173','136']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-actions=''
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;rZ:-90;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 100px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;text-decoration: none;"> Scroll Down  </a>
					</li>
					<!-- SLIDE  -->
					<li data-index="rs-68" data-transition="slidingoverlayright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="assets/revslider1/assets/100x50_93b58-slider2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
						<!-- MAIN IMAGE -->
						<img src="{{ asset('assets/frontend/revslider1/assets/93b58-slider2.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 6 -->
						<div class="tp-caption"
							id="slide-68-layer-3"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-72','-47']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;"><strong>Autokosmetik</strong> the agency</div>
						<!-- LAYER NR. 7 -->
						<div class="tp-caption"
							id="slide-68-layer-4"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">creative <strong>and innovative</strong> </div>
						<!-- LAYER NR. 8 -->
						<div class="tp-caption"
							id="slide-68-layer-5"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['81','83','70','45']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">of the entire <strong>network</strong> </div>
						<!-- LAYER NR. 9 -->
						<div class="tp-caption"
							id="slide-68-layer-7"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-143','-147','-138','-91']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 8; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><i class="pe-7s-glasses"></i> </div>
						<!-- LAYER NR. 10 -->
						<a class="tp-caption scroll-down"
							href="#section-aboutus1" target="_self"	id="slide-68-layer-9"
							data-x="['right','right','right','right']" data-hoffset="['-38','-38','-38','-38']"
							data-y="['middle','middle','middle','middle']" data-voffset="['188','196','173','136']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-actions=''
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;rZ:-90;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 100px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;text-decoration: none;"> Scroll Down  </a>
					</li>
					<!-- SLIDE  -->
					<li data-index="rs-69" data-transition="slidingoverlayright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="{{ asset('assets/frontend/revslider1/assets/100x50_30d37-slider5.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
						<!-- MAIN IMAGE -->
						<img src="{{ asset('assets/frontend/revslider1/assets/30d37-slider5.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 11 -->
						<div class="tp-caption"
							id="slide-69-layer-3"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-72','-47']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;"><strong>Autokosmetik</strong>  is the perfect </div>
						<!-- LAYER NR. 12 -->
						<div class="tp-caption"
							id="slide-69-layer-4"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">for all <strong>types</strong> </div>
						<!-- LAYER NR. 13 -->
						<div class="tp-caption"
							id="slide-69-layer-5"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['81','83','70','45']"
							data-fontsize="['60','60','50','30']"
							data-lineheight="['80','80','70','40']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">of <strong>solutions</strong> </div>
						<!-- LAYER NR. 14 -->
						<div class="tp-caption"
							id="slide-69-layer-7"
							data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-143','-147','-138','-91']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 8; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><i class="pe-7s-glasses"></i> </div>
						<!-- LAYER NR. 15 -->
						<a class="tp-caption scroll-down"
							href="#section-aboutus1" target="_self"			 id="slide-69-layer-9"
							data-x="['right','right','right','right']" data-hoffset="['-38','-38','-38','-38']"
							data-y="['middle','middle','middle','middle']" data-voffset="['188','196','173','136']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-type="text"
							data-actions=''
							data-responsive_offset="on"
							data-responsive="off"
							data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;rZ:-90;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							data-textAlign="['inherit','inherit','inherit','inherit']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"
						style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 100px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;text-decoration: none;"> Scroll Down  </a>
					</li>
				</ul>
				<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
			</div>
			<!-- /.Revolution Slider 1 -->
			<!-- Setion About us -->
			<div id="section-aboutus1" class="bg-dark-autokosmetic">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-6" data-aos="fade-up" data-aos-anchor-placement="top-center">
							<h6 >About Us</h6>
							<h1>Welcome to <span>Autokosmetik</span> is a <span>Specialized</span> Design Agency.</h1>
							<p>at the leading automobile finisher in the Principality of Liechtenstein / Switzerland Rhine Valley region. Regardless of which product or service you buy from us, you can assume that you can expect optimum quality. At a fair price.
							<br>
							Don't hesitate to call us with any questions or just drop by Gamprin for a coffee.<span class="text-right"> - Pascal & Rudi Nitzlnader</span>
								<br>
							</p>
							<a href="#" class="btn-3">Continue <i class="fa fa-chevron-right"></i></a>
							<br><br>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-aos="fade-left">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-aboutus-1.png') }}" alt="autokosmetik">
						</div>
						<div class="bottom ez-animate col-12" data-aos="fade-right">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-aboutus-2.png') }}" alt="autokosmetik">
							<label>Founder Autokosmetik </label>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Setion About us -->
			<!-- Section miscellaneous 1 -->
			<div id="section-miscellaneous1-1" class="feature-car-area white-bg page-section-pt">
				<div class="container">
					<div class="row">
						<!-- <div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-heart"></i>
							<h3 class="counter-value count">288.702</h3>
							<p>Customers</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-stopwatch"></i>
							<h3 class="counter-value count">140.545</h3>
							<p>Works completes</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-internet"></i>
							<h3 class="counter-value count">100.204</h3>
							<p>Projects of design</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-id-card"></i>
							<h3 class="counter-value count">22.368</h3>
							<p>Companies</p>
						</div> -->
						<div class="row">
							<div class="col-lg-4 col-md-6 align-self-center">
								<div class="feature-box-2 text-lg-right text-left" data-aos="fade-right" data-aos-delay="600">
									<div class="icon-img icon-img-left">
										<img src="{{ asset('assets/frontend/images/flaticon/sedan.svg') }}" alt="">
									</div> 
									<div class="content">
										<h5>Super fast </h5>
										<p>Leap lorem Ipsum is simply dummy text the implications printin  k a galley of.</p>
									</div>
								</div>
								<div class="feature-box-2 text-lg-right text-left" data-aos="fade-right" data-aos-delay="800">
									<div class="icon-img icon-img-left">
									<img src="{{ asset('assets/frontend/images/flaticon/wallet.svg') }}" alt="">
									</div> 
									<div class="content">
										<h5>AFFORDABLE</h5>
										<p>To realize why this exercise implications is called the printin Dickens of.</p>
									</div>
								</div>
								<div class="feature-box-2 text-lg-right text-left" data-aos="fade-right" data-aos-delay="1000">
									<div class="icon-img icon-img-left">
										<img src="{{ asset('assets/frontend/images/flaticon/gas-station.svg') }}" alt="">
									</div> 
									<div class="content">
										<h5>OIL CHANGES </h5>
										<p>Called the to realize why this implications exercise is printin Dickens of.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 d-lg-block d-none">
								<div class="service-img-car"  data-aos="fade-up" data-aos-delay="200">
									<img class="img-fluid center-block big-car" src="{{ asset('assets/frontend/images/service-5.png') }}" alt="">
								</div>
							</div>
							<div class="col-lg-4 col-md-6 align-self-center">
								<div class="feature-box-2" data-aos="fade-left" data-aos-delay="600">
									<div class="icon-img">
										<img src="{{ asset('assets/frontend/images/flaticon/air-conditioning.svg') }}" alt="">
									</div> 
									<div class="content">
										<h5>AIR conditioning </h5>
										<p>Printin Dickens called the to realize implications why this exercise is of.</p>
									</div>
								</div>
								<div class="feature-box-2" data-aos="fade-left" data-aos-delay="800">
									<div class="icon-img">
										<img src="{{ asset('assets/frontend/images/flaticon/gearstick.svg') }}" alt="">
									</div> 
									<div class="content">
										<h5>transmission</h5>
										<p>Text the printin leap v lorem Ipsum is simply dummy k a galley of.</p>
									</div>
								</div>
								<div class="feature-box-2" data-aos="fade-left" data-aos-delay="1000">
									<div class="icon-img">
										<img src="{{ asset('assets/frontend/images/flaticon/car-key.svg') }}" alt="">
									</div> 
									<div class="content">
										<h5>DEALERSHIP</h5>
										<p>Dickens printin called the to implications realize why this exercise is of.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Section miscellaneous 1 -->
			<!-- Section section-recentworks 1 -->
			<div id="section-recentworks1" class="bg-dark-autokosmetic">
				<div class="container">
					<div class="row">
						<div class="title1 ez-animate col-12" data-animation="fadeInUp">
							<h6>RECENT WORKS</h6>
							<h2>Our Portfolio</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="our-portfolio col-12">
							<div class="row">
								<div class="item col-12 col-md-4">
									<a href="#">
										<div class="img-container">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/recentworks-1.jpg') }}" alt="autokosmetik">
											<div class="overlay">
												<div class="overlay-content">
													<i></i>
													<h3>Autocosmetics</h3>
													<p>Photography</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="item col-12 col-md-4">
									<a href="#">
										<div class="img-container">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/recentworks-1.jpg') }}" alt="autokosmetik">
											<div class="overlay">
												<div class="overlay-content">
													<i></i>
													<h3>Autocosmetics</h3>
													<p>Photography</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="item col-12 col-md-4">
									<a href="#">
										<div class="img-container">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/recentworks-3.jpg') }}" alt="autokosmetik">
											<div class="overlay">
												<div class="overlay-content">
													<i></i>
													<h3>Autocosmetics</h3>
													<p>Photography</p>
												</div>
											</div>
										</div>
									</a><a href="#">
										<div class="img-container">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/recentworks-4.jpg') }}" alt="autokosmetik">
											<div class="overlay">
												<div class="overlay-content">
													<i></i>
													<h3>Autocosmetics</h3>
													<p>Photography</p>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Section section-recentworks 1 -->
			<!-- Section CTA 1-->
			<div id="section-cta1">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-9" data-aos="fade-up" data-aos-delay="200">
							<h2>You need a more impressive than this, <span>contact us now</span></h2>
						</div>
						<div class="right col-sm-12 col-md-3" data-aos="fade-up" data-aos-delay="400">
							<a href="#" class="btn-2">Buy this now</a>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Section CTA 1-->
			<!-- Section Testimonial 1 -->
			<div id="section-testimonial1">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<h6>CLIENTS SAY</h6>
								<h2>Our Client Feedback</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme">
								<!-- Item -->
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}"alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Katie Pavardenis</h6>
										<p>Ceo of Enva</p>
									</div>
								</div>
								<!-- /.Item -->
								<!-- Item -->
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial2.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Katie Pavardenis</h6>
										<p>Ceo of Enva</p>
									</div>
								</div>
								<!-- /.Item -->
								<!-- Item -->
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial3.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Katie Pavardenis</h6>
										<p>Ceo of Enva</p>
									</div>
								</div>
								<!-- /.Item -->
							</div>
						</div>
					</div>
				</div>
				<div class="testiomonial-parallax-bg">
					<img src="{{ asset('assets/frontend/images/parallax-bg.jpg') }}" alt="">
					<!-- <div class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/frontend/images/parallax-bg.jpg"></div> -->
				</div>
			</div>
			<!-- /.Section Testimonial 1 -->
			<!-- Section Contact Form 1 -->
			<div id="section-contactform1">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<h6>CONTACT FORM</h6>
								<h2>Get In Touch</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="contactform1">
								<form id="contactform1" action="#">
									<div class="form-row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="name" placeholder="Name *" required>
										</div>
										<div class="form-group col-md-6">
											<input type="email" class="form-control" id="email" placeholder="Email *" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="PhoneNumber" placeholder="Phone Number *" required>
										</div>
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="subject" placeholder="Subject *" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<textarea class="form-control" id="message" rows="3" placeholder="Message *" required></textarea>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group m-bot0 col-md-12">
											<button type="submit">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Section Contact Form 1 -->
        </div>
    </div>
@endsection    
	