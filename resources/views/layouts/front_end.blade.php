<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Autokosmetik" />
        <!-- Page Kewords -->
        <meta name="keywords" content="Autokosmetik" />
        <!-- Site Author -->
        <meta name="author" content="Autokosmetik" />
         <!-- Page Title Here -->
        <title>{{ env('APP_NAME', 'Autokosmetik') }} | @yield('title')</title>

        <link rel="icon" href="{{ asset('assets/frontend/images/favicon.ico') }}" type="image/x-icon" >

        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!-- Bootstrap 4 -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" type="text/css">
        <!-- Swiper Slider -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/swiper.min.css') }}" type="text/css">
        <!-- Fonts -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/fonts/flaticon/flaticon.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/frontend/fonts/fontawesome/font-awesome.min.css') }}" type="text/css">
        <!-- OWL Carousel -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.carousel.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.theme.default.min.css') }}" type="text/css">
        
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}" type="text/css">
        <!-- REVOLUTION SLIDER -->
        <link href="http://fonts.googleapis.com/css?family=Quicksand:400%2C700%7COpen+Sans:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
        <link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/fonts/font-awesome/css/font-awesome.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/settings.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/inline-revslider2.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/revolution.addon.particles.css') }}" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/revolution.addon.polyfold.css') }}" type="text/css"  media="all" />
        <link rel="stylesheet" href="https://cdn.rawgit.com/michalsnik/aos/2.0.4/dist/aos.css" type="text/css">
    </head>
    <body>
        <!-- Header.php -->
        <!-- Section Preloader -->
        <div id="section-preloader">
            <img src="{{ asset('assets/frontend/images/loader1.gif') }}" alt="">
        </div>
        <!-- /.Section Preloader -->
        <!-- Search -->
        <div id="section-search" class="close-search">
            <div class="container">
                <div class="navigation-search">
                    <div class="row">
                        <div class="col ns-logo">
                            <a href="index.php"><img src="{{ asset('assets/frontend/images/logo.png') }}" alt="autokosmetik"></a>
                        </div>
                        <div class="col ns-close">
                            <span class="close"></span>
                        </div>
                    </div>
                </div>
                <div class="col search-content">
                    <form action="#" method="post">
                        <input type="text" name="search" placeholder="Type here give it to enter">
                        <input type="submit" name="search" value="">
                        <i class="flaticon-search"></i>
                    </form>
                </div>
            </div>
        </div>
        <!-- Section Navbar -->
        <div id="section-navbar1">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <div class="header-logo mr-auto">
                        <a href="index.php"><img src="{{ asset('assets/frontend/images/logo.png') }}" alt="Akroot"></a>
                    </div>
                    <ul class="nav navbar-nav navbar1">
                        <li class="nav-item">
                            <div class="header-contact"><i class="fa fa-phone text-light"></i><span><a href="callto:+423 373 03 54">&nbsp;&nbsp;+423 373 03 54</a></span></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link search-btn" href="#"><span class="flaticon-search"></span></a>
                        </li>
                        <li class="nav-item">
                            <button type="button" id="sidebarCollapse" class="navbar-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /.Section Navbar -->
        <nav id="sidebar" class="active">
            <ul class="list-unstyled components">
                <li>
                    <a href="{{ route('index')}}" aria-expanded="false" class="list-menu">Home</a>
                </li>
                <li>
                    <a href="{{ route('about.us')}}" class="list-menu">About</a>
                </li>
                <li>
                    <a href="{{ route('services')}}" class="list-menu">Services</a>
                </li>
                <li>
                    <a href="{{ route('news')}}" class="list-menu">News</a>
                </li>
                <li>
                    <a href="{{ route('portfolio')}}" class="list-menu">Portfolio</a>
                </li>
                <li>
                    <a href="{{ route('contact')}}" class="list-menu">Contact</a>
                </li>
            </ul>
        </nav>
        <section class="main-content">
            @yield('content')
        </section>
        <footer>
            <div id="section-footer">
                <div class="footer1 col-12">
                    <div class="widget col-12">
                        <a href="index.php"><img src="{{ asset('assets/frontend/images/logo.png') }}" alt="autokosmetik"></a>
                    </div>
                </div>
            </div>
            <div id="section-footer" class="sec-footer-bg">
                <div class="container">
                    <div class="footer2 row">
                        <div class="ft-copyright col-md-6">
                            <p>Copyright © 2021 <a href="https://www.applie.li/" target="_blank" rel="noopener noreferrer">Applie AG.</a> All Rights Reserved.</p>
                        </div>
                        <div class="ft-socialmedia col-md-6">
                            <div class="social-links socials-header">
                                <a href="https://www.facebook.com/AUTOKOSMETIK/"><i class="fa fa-facebook fa-lg"></i></a>
                                <a href="https://www.google.ch/maps/place/Autokosmetik.li/@47.218252,9.511339,15z/data=!4m5!3m4!1s0x0:0x3025938a37e377f0!8m2!3d47.218252!4d9.511339"><i class="fa fa-map-marker fa-lg"></i></a>
                                <a href="mailto:info@autokosmetik.li"><i class="fa fa-envelope fa-lg"></i></a>
                                <a href="callto:+423 373 03 54"><i class="fa fa-phone fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="car-top show car-down">
            <span><img src="{{ asset('assets/frontend/images/car.png') }}" alt=""></span>
        </div>
        <script src="{{ asset('assets/frontend/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>

        <script src="{{ asset('assets/frontend/js/owl.carousel.min.js') }}"></script>
        <!-- SLY Carousel -->
        <script src="{{ asset('assets/frontend/js/sly.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/sly.vendor.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.14/swiper-bundle.min.js"></script>
        <script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/custom-2.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/sly-ourportfolio.js') }}"></script>
        <script src="{{ asset('assets/frontend/revslider2/js/revolution.addon.particles.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/revslider2/js/revolution.addon.snow.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/revslider2/js/revolution.addon.polyfold.min.js') }}"></script>
        <!-- REVOLUTION JS FILES -->
        <script src="{{ asset('assets/frontend/revslider2/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/revslider2/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/revslider1/js/inline-revslider1.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.2.0/dist/simpleParallax.min.js"></script>
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.0.4/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
            $('#section-portfoliodetails1 .owl-carousel').owlCarousel({
                stagePadding: 300,
                // loop:true,
                margin:10,
                nav:false,
                items:3,
                autoplay:true,
                responsive:{
                    0:{
                        items:1,
                        stagePadding: 20,
                    },
                    600:{
                        items:1,
                        stagePadding: 20,
                    },
                    1280:{
                        items:1,
                        stagePadding: 200,
                    },
                    1920:{
                        items:1
                    }
                }
            })

            $(document).ready(function() {
                
                $('.button').click(function(){
                    

                    var buttonId = $(this).attr('id');
                    $('#modal-container').removeAttr('class').addClass(buttonId);
                    $('body').addClass('modal-active');
                })
                $('#modal-container').click(function(){
                    $(this).addClass('out');
                    $('body').removeClass('modal-active');
                });

                $('.count').each(function () {
                        $(this).prop('Counter',0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 4000,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        });
                    });
            });


            const navSlide = () => {
            const burger = document.querySelector('.navbar-btn');
            const nav = document.querySelector('.list-unstyled');
            const navLink = document.querySelectorAll('.list-unstyled li')

                burger.addEventListener('click', () => {
                    
                    nav.classList.toggle('nav-active');
                    
                    navLink.forEach((link, index) => {
                        if (link.style.animation) {
                            link.style.animation = ''
                        } else {
                            link.style.animation = `navLinkFade 2s ease forwards ${index / 7 + 0.2}s`;
                        }
                    });

                    burger.classList.toggle('toggle');
                })

            }

            navSlide();

            $(document).ready(function() {
                    
                var image = document.getElementsByClassName('thumbnail_1');
                new simpleParallax(image, {
                    scale: 1.5,
                });
            });
        </script>
    </body>
</html>


