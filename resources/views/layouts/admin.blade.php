<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Autokosmetik') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/backend/images/favicon.ico') }}">

    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/backend/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />

    <!-- Icons Css -->
    <link href="{{ asset('assets/backend/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/css/custom.css') }}" rel="stylesheet" type="text/css" />

    <!-- DataTables -->
    <link href="{{ asset('assets/backend/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/backend/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- CSS only -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->


</head>

<body data-sidebar="dark">
    <!-- Begin page -->
    <div id="app">

        @include("partials.topbar")
        @include("partials.sidebar")
        <section class="main-content">
            @yield('content')
        </section>
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        © <script>
                            document.write(new Date().getFullYear())
                        </script>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Appile AG
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- END layout-wrapper -->
    <!-- JAVASCRIPT -->
    <script src="{{ asset('assets/backend/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/backend/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/backend/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/backend/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/backend/libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/jquery.validate.js') }}"></script>
    <!-- Required datatable js -->
    <script src="{{ asset('assets/backend/libs/datatables.net/js/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('assets/backend/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>

    <!-- Swwtalert Js -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

    <!-- apexcharts -->
    <!-- <script src="{{ asset('assets/backend/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/pages/dashboard.init.js') }}"></script>-->

    <!-- App js -->
    <script src="{{ asset('assets/backend/js/app.js') }}"></script>

    @yield('scripts')
    @php
    $routeName = Route::currentRouteName();

    $route = str_replace(".index","",$routeName);
    $all_routes =
    array("products");
    @endphp

    @if(in_array($route, $all_routes))

    <script>
        $(function() {
            var _token = $('meta[name="csrf-token"]').attr('content');
            var table = $('.datatable-User').DataTable();
            $('body').on('click', '.switch_checkbox_btn', function() {
                var ids = $(this).attr('entry-id');
                jQuery('.main-loader').show();
                $.ajax({
                        headers: {
                            'x-csrf-token': _token
                        },
                        method: 'POST',
                        url: "{{ route($route.'.switchUpdate'," + ids + ") }}",
                        data: {
                            ids: ids,
                            _method: 'POST'
                        }
                    })
                    .done(function() {
                        jQuery('.main-loader').hide();
                    })
            });
            $('body').on('click', '.action-delete', function() {
                var current = $(this);
                var ids = $(this).attr('entry-id');
                Swal.fire({
                    icon: 'warning',
                    title: "Are you sure delete this?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, delete it!",
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                                headers: {
                                    'x-csrf-token': _token
                                },
                                method: 'POST',
                                url: "{{ route($route.'.destroy'," + ids + ") }}",
                                data: {
                                    ids: ids,
                                    _method: 'DELETE'
                                }
                            })
                            .done(function() {
                                Swal.fire("Deleted!", "Successfully.",
                                    "success");
                                table.row(current.parents('tr'))
                                    .remove()
                                    .draw();
                            })
                    }
                });
            });
        });
    </script>
    @endif
</body>

</html>