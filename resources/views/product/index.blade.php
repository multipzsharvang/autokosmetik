@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>Products Mgmt</h4>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('products.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add new product</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                @if (session('success'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('error') }}
                </div>
                @endif

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Short Description</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $key => $product)
                                    <tr data-entry-id="{{ $product->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $product->title ?? ''}}</td>
                                        <td>
                                            {!! $product->short_description !!}</td>
                                        <td>
                                            @if($product->image)
                                                <a href="{{ asset(Storage::url($product->image)) }}" target="_blank">
                                                    <img src="{{ asset(Storage::url($product->image)) }}" alt="" height=50 width=50>
                                                </a>
                                            @else
                                                '-'
                                            @endif
                                        </td>
                                        <td>{{ $product->category ?? ''}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $product->id,'is_active'=>$product->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $product->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
