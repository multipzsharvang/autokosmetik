@section('title', 'News')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
		    <div id="section-breadcrumb1"  class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
		    	<div class="container">
		    		<div class="row">
		    			<div class="content col-12">
		    				<h1>News</h1>
		    				<ul>
		    					<li><a href="{{ route('index')}}">HOME</a></li>
		    					<li class="current text-light">NEWS</li>
		    				</ul>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div id="section-news2">
				<div class="container">
					<div class="row">
						<div class="contents col-12 col-sm-12 col-md-12 col-lg-8">
							<!-- Item -->
							<div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-1.jpg') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 27, 2019</span>
								<a href="#"><h2>WINDOW TINTING WITH TEMPERATURE DISCOUNT</h2></a>
							</div>
							<!-- /.Item -->
							<!-- Item -->
							<div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-2.jpg') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 25, 2019</span>
								<a href="#"><h2>OPEN DAY, MARCH 18, 10 A.M. - 4 P.M.</h2></a>
							</div>
							<!-- /.Item -->
							<!-- Item -->
							<div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-3.png') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 18, 2019</span>
								<a href="#"><h2>CERAMIC COATING FOR MOTORCYCLES - WINTER PROMOTION</h2></a>
							</div>
							<!-- /.Item -->
							<!-- Item -->
							<div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-4.jpg') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 18, 2019</span>
								<a href="#"><h2>WINDOW TINTING CAMPAIGN JANUARY 2017</h2></a>
							</div>
							<!-- /.Item -->
							<!-- Item -->
							<div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-5.png') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 18, 2019</span>
								<a href="#"><h2>CAR PAINT CAMPAIGN</h2></a>
							</div>
							<!-- /.Item -->
                            <div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-6.jpg') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 18, 2019</span>
								<a href="#"><h2>MERCEDES FOILING</h2></a>
							</div>
							<!-- /.Item -->
                            <div class="item">
								<a href="#"><img class="img-fluid" src="{{ asset('assets/frontend/images/news-7.jpg') }}" alt="autokosmetik"></a><br>
								<span>SEPTEMBER 18, 2019</span>
								<a href="#"><h2>REPRESENTATION OF SERVFACES</h2></a>
							</div>
							<!-- /.Item -->

							<!-- Pagination -->
							<ul class="pagination justify-content-center">
								<li class="page-item disabled">
									<span class="page-link">prev</span>
								</li>
								<li class="page-item"><a class="page-link" href="#">01</a></li>
								<li class="page-item"><a class="page-link" href="#">02</a></li>
								<li class="page-item active" aria-current="page">
									<span class="page-link">
										03
										<span class="sr-only">(current)</span>
									</span>
								</li>
								<li class="page-item"><a class="page-link" href="#">...</a></li>
								<li class="page-item"><a class="page-link" href="#">10</a></li>
								<li class="page-item">
									<a class="page-link" href="#">next</a>
								</li>
							</ul>
							<!-- /.Pagination -->
						</div>
						<div class="sidebar col-12 col-sm-12 col-md-12 col-lg-4">
							<div class="searchbar">
								<h3>Search Section</h3>
								<form action="#" method="post">
				                    <input type="text" name="search" placeholder="Search autokosmetik">
				                    <i class="flaticon-search"></i>
				                </form>
							</div>
							<div class="popular-news">
								<h3>Popular News</h3>
								<div class="list">
									<a href="#">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/img-popularnews1-1.jpg') }}" alt="autokosmetik">
										<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
									</a>
								</div>
								<div class="list">
									<a href="#">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/img-popularnews1-2.jpg') }}" alt="autokosmetik">
										<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
									</a>
								</div>
								<div class="list">
									<a href="#">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/img-popularnews1-3.jpg') }}" alt="autokosmetik">
										<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
									</a>
								</div>
								<div class="list">
									<a href="#">
										<img class="img-fluid" src="{{ asset('assets/frontend/images/img-popularnews1-4.jpg') }}" alt="autokosmetik">
										<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
									</a>
								</div>
							</div>
							<div class="instagram-gallery">
								<h3>Instagram Gallery</h3>
								<div class="row">
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-1.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-2.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-3.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-4.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-5.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-6.jpg') }}" alt="autokosmetik">
										</a>
									</div>
								</div>
							</div>
							<div class="category">
								<h3>Category</h3>
								<ul>
									<li><a href="#">Business</a></li>
									<li><a href="#">Tegnology</a></li>	
									<li><a href="#">Creative TF</a></li>
									<li><a href="#">Design</a></li>
									<li><a href="#">Illustrations</a></li>
									<li><a href="#">Graphic Art</a></li>
								</ul>
								<ul>
									<li><a href="#">Web developer</a></li>
									<li><a href="#">Marketing</a></li>	
									<li><a href="#">Audio edition</a></li>
									<li><a href="#">Photoshop</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-subscribe1">
				<div class="container">
					<div class="row">
						<div class="title1 col-12">
							<h2><span>Stay connected to our</span> Newsletters</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="content col-12">
							<p>Aliquam vehicula mollis urna vel dignissim. Integer tincidunt viverra est, non congue lorem tempor ac. Phasellus pulvinar iaculis.</p>
						</div>
						<div class="subscribe col-12">
							<form action="#" id="SubscribeForm">
								<input type="email" name="yourEmail" placeholder="Email Address" required>
								<button type="submit">Subscribe</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	