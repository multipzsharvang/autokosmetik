@extends('layouts.app')

@section('content')
<div class="account-pages my-5 pt-sm-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-soft-primary">
                        <div class="row">
                            <div class="col-7">
                                <div class="text-primary p-4">
                                    <h5 class="text-primary">Welcome Back !</h5>
                                    <p>Sign in to continue to Autokosmetik.</p>
                                </div>
                            </div>
                            <div class="col-5 align-self-end mb-2">
                                <img src="{{ asset('assets/frontend/images/logo.png') }}" alt=""
                                    class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="p-2">
                            @if(\Session::has('message'))
                                <p class="alert alert-info">
                                    {{ \Session::get('message') }}
                                </p>
                            @endif
                            <form class="form-horizontal" action="{{ route('login') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input name="email" type="text"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        autofocus placeholder="Email"
                                        value="{{ old('email', null) }}">
                                    @if($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userpassword">Password</label>
                                    <input name="password" type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        placeholder="Password">
                                    @if($errors->has('password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input"
                                        id="customControlInline">
                                    <label class="custom-control-label" for="customControlInline">Remember
                                        me</label>
                                </div>

                                <div class="mt-3">
                                    <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log
                                        In</button>
                                </div>

                                <!-- <div class="mt-4 text-center">
                                        <a href="auth-recoverpw.html" class="text-muted"><i
                                                class="mdi mdi-lock mr-1"></i> Forgot your password?</a>
                                    </div> -->

                            </form>
                        </div>

                    </div>
                </div>
                <div class="mt-4 text-center">
                    <!-- <p>Don't have an account ? <a href="auth-register.html" class="font-weight-medium text-primary">
                                Signup now </a> </p>  -->
                    <p>© <script>
                            document.write(new Date().getFullYear())
                        </script>, Designed & Developed <i class="mdi mdi-heart text-danger"></i> by <a
                            href="https://www.multipz.com/" target="_blank">Appile AG</a></p>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
