    <aside class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <!-- Admin show menu start -->


                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">Menu</li>
                    <li  class="{{ request()->is('admin/services') || request()->is('admin/services/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('services.index') }}" class="waves-effect {{ request()->is('services') || request()->is('admin/services/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>Service Mgmt</span>
                        </a>
                    </li>
                </ul>
                <!-- Admin show menu end -->
            </div>
            <!-- Sidebar -->
        </div>
    </aside>
    <!-- Left Sidebar End -->
