@section('title', 'Services')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">				
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Our Services</h1>
							<ul>
								<li><a href="{{ route('index')}}">HOME</a></li>
								<li class="current text-light">OUR SERVICES</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services1">
				<div class="container">
					<div>
						<ul class=" d-flex align-items-center modal-main">
							<li class="button" id="one">
								<a  class="service-hover collapsed ">
									<div class="service-type p-3">
										<i class="flaticon-like">
										</i>
										<h5 class="pt-3">Car Cosmetics</h5>
									</div>
								</a>
							</li>
							<li>
								<a data-toggle="collapse" href="#foiltech" role="button" aria-expanded="false" aria-controls="foiltech" class="service-hover collapsed" data-link="foiltech">
									<div class="service-type p-3">
										<i class="flaticon-layers"></i>
										<h5 class="pt-3">Foil Technology</h5>
									</div>
								</a>
							</li>
							<li>
								<a data-toggle="collapse" href="#rimtire" role="button" aria-expanded="false" aria-controls="rimtire" class="service-hover collapsed" data-link="rimtire">
									<div class="service-type p-3">
										<i class="flaticon-fingerprint"></i>
										<h5 class="pt-3">Rim & Tire Service</h5>
									</div>
								</a>
							</li>
							<li role="presentation">
								<a data-toggle="collapse" href="#voucher" role="button" aria-expanded="false" aria-controls="voucher" class="service-hover collapsed" data-link="voucher">
									<div class="service-type p-3">
										<i class="flaticon-worldwide"></i>
										<h5 class="pt-3">Voucher</h5>
									</div>
								</a>
							</li>
							<div id="modal-container">
								<div class="modal-background">
									<div class="modal p-lg-5">
										<div id="section-portfoliodetails1" class="section-portfoliodetails1-padding">
											<div id="" class="container tab-content" >
												<div id="carcosmetics" class="collapse show" data-parent="#mainCollapse" style="">
													<div class="row">
														<div class="related-projects col-12">
															<h2 class="pb-4">Car Cosmetics</h2>
															<div class="row">
																<!-- Item -->
																<div class="item col-sm-12 col-md-4">
																	<a href="ceramic-coating.php">
																		<div class="img-container">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-1.jpg') }}" alt="autokosmetik">
																			<div class="overlay">
																				<div class="overlay-content">
																					<h3>EXTERIOR PREPARATION CERAMIC COATING</h3>
																					<p>The armored glass-like paint protection <br>Shelf life 36+ months <br>Car wash festival</p>
																					<i></i>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4">
																	<a href="#">
																		<div class="img-container">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-2.jpg') }}" alt="autokosmetik">
																			<div class="overlay">
																				<div class="overlay-content">
																					<h3>EXTERIOR PREPARATION TEFLON SEAL</h3>
																					<p>The standard paint protection <br>Shelf life 6+ months <br>Inexpensive</p>
																					<i></i>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4">
																	<a href="#">
																		<div class="img-container">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-3.jpg') }}" alt="autokosmetik">
																			<div class="overlay">
																				<div class="overlay-content">
																					<h3>EXTERIOR PREPARATION ACCORDING TO SWISSVAX</h3>
																					<p>Carnauba waxes from Swissvax <br>Shelf life 6+ months <br>Optimal show shine</p>
																					<i></i>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 pt-5">
																	<a href="#">
																		<div class="img-container">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
																			<div class="overlay">
																				<div class="overlay-content">
																					<h3>CONVENTIONAL INTERIOR PREPARATION</h3>
																					<p>including shampooing <br>Leather cleaning and care <br>Cleaning with Tornador</p>
																					<i></i>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 pt-5">
																	<a href="#">
																		<div class="img-container">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-5.jpg') }}" alt="autokosmetik">
																			<div class="overlay">
																				<div class="overlay-content">
																					<h3>INTERIOR PREPARATION ACCORDING TO SWISSVAX</h3>
																					<p>high quality cleaning agents <br>optimal long-term care <br>exclusively Swissvax products</p>
																					<i></i>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 pt-5">
																	<a href="#">
																		<div class="img-container">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-6.jpg') }}" alt="autokosmetik">
																			<div class="overlay">
																				<div class="overlay-content">
																					<h3>INTERIOR SPECIAL CASES</h3>
																					<p>Dog hair <br>Odor treatments <br>Mold, etc.</p>
																					<i></i>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</ul>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-service-text-color">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-6">
							<h1>Analysis and cleaning of the link profile</h1>
							<p>
								Lorem ipsum, dolor sit amet consectetur adipisicing elit. Esse ut modi maiores quam quibusdam in quod explicabo tenetur. <br><br>
								Eum eligendi, veritatis voluptatum laboriosam modi ut sequi nesciunt consectetur veniam reiciendis?
							</p>
							<a href="#" class="btn-3">Start free trial <i class="fa fa-chevron-right"></i></a>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid w-75 img-tire-service" src="{{ asset('assets/frontend/images/tire.png') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 bg-2 bg-dark-autokosmetic">
				<div class="container">
					<div class="row">
						<div class="right ez-animate col-sm-12 col-md-6 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-services2.png') }}" alt="autokosmetik">
						</div>
						<div class="left col-sm-12 col-md-6">
							<h1>SEM Digital campaigns</h1>
							<p>
								Lorem ipsum, dolor sit amet consectetur adipisicing elit. Esse ut modi maiores quam quibusdam in quod explicabo tenetur. <br><br>
								Eum eligendi, veritatis voluptatum laboriosam modi ut sequi nesciunt consectetur veniam reiciendis?
							</p>
							<a href="#" class="btn-3">Start free trial <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 section-service-text-color">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-6">
							<h1 class="mw-100">SEO Positioning strategy</h1>
							<p>
							Lorem ipsum, dolor sit amet consectetur adipisicing elit. Esse ut modi maiores quam quibusdam in quod explicabo tenetur. <br><br>
								Eum eligendi, veritatis voluptatum laboriosam modi ut sequi nesciunt consectetur veniam reiciendis?
							</p>
							<a href="#" class="btn-3">Start free trial <i class="fa fa-chevron-right"></i></a>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-services3.png') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div id="section-howwework">
				<div class="container">
					<div class="row">
						<div class="col-12 text-center">
							<i class="flaticon-multimedia"></i>
							<h1>How we <strong>Work</strong></h1>
						</div>
					</div>
				</div>
			</div>
			<div id="section-cta3">
				<div class="container">
					<div class="row">
						<div class="title1 col-12">
							<h2><span>autokosmetik is the most impressive team you were</span> Looking For</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="cta-content ez-animate col-12" data-animation="fadeInUp">
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod error quas deserunt. Veniam ipsum quas est? Necessitatibus impedit commodi, quaerat doloribus, praesentium molestiae ad odit tempore quidem illum possimus harum!</p>
							<a href="#" class="btn-4">Buy now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	