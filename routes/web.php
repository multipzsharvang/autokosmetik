<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);
Route::get('/login', function () {
    return redirect('admin/login');
});
Route::get('/register',function () {
    return redirect('admin/login');
});

Route::post('logout', 'Auth\LoginController@logout');


Route::get('/', 'WebsiteController@index')->name('index');
Route::get('/index', 'WebsiteController@index')->name('index');
Route::get('/about-us', 'WebsiteController@aboutUs')->name('about.us');
Route::get('/news', 'WebsiteController@news')->name('news');
Route::get('/services', 'WebsiteController@services')->name('services');
Route::get('/car-cosmetics', 'WebsiteController@carCosmetics')->name('car.cosmetics');
Route::get('/foil-technology', 'WebsiteController@foilTechnology')->name('foil.technology');
Route::get('/rim-tire', 'WebsiteController@rimTire')->name('rim.tire');
Route::get('/voucher', 'WebsiteController@voucher')->name('voucher');
Route::get('/contact', 'WebsiteController@contact')->name('contact');
Route::get('/portfolio', 'WebsiteController@portfolio')->name('portfolio');



// Route::get('/product', 'WebsiteController@product')->name('product');

// Route::get('/contact',  'WebsiteController@contact')->name('contact');

// Route::get('/work',  'WebsiteController@work')->name('work');


// Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' =>'admin'], function () {
	Auth::routes();

	Route::get('/', function () {
    	return redirect('admin/products');
	});
	Route::get('/register',function () {
    	return redirect('admin/products');
	});

	

	Route::group(['middleware' => 'auth'], function () {
		Route::post('products/switchUpdate', 'ProductController@switchUpdate')->name('products.switchUpdate');
    	Route::resource('/products', 'ProductController');
    	Route::post('/product/updateStatus', 'ProductController@toggleStatus')->name('product.toggleStatus');
	});
});


